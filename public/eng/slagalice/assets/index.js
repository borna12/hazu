var slika, retci, stupci, opis;


function broj() {
    $("#broj-puzzla").html($("#retci").val() * $("#stupci").val())
}



$(document).ready(function() {
    $(".modal").fadeIn("slow");
    $("#broj-puzzla").html(retci * stupci)
    broj();
    $('select').css({
        'font-size': '18px',
        'font-family': "Georgia"
    });


});



function stvori() {
    if (slika == '1.jpg') {
        opis = "<p>Getaldić began writing his two most mature works, <em>Variorum problematum collectio</em> (<em>A Collection of Various Problems</em>; Venice, 1607) and <em>De resolutione et compositione mathematica</em> (<em>On Mathematical Analysis and Synthesis</em>; Rome, 1630) at the same time, with the goal of using exclusively mathematical methods from classical antiquity in the first, with the second affirming Vi&egrave;te&rsquo;s symbolic algebra by applying it to various problems.</p><p><em>Variorum problematum</em> is a methodologically very important work because it reflects his first step towards the analytical solution of problems. <em>De resolutione </em>was the first complete handbook of new algebraic analysis. Getaldić worked on it towards the end of his life. His methods were completely innovative and demonstrated a broad and general application of methods of algebraic analysis and synthesis on mathematical and physical problems. The work consists of five volumes. The first and second volume contained algebraic solutions to Vi&egrave;te&rsquo;s and Euclid&rsquo;s problems that Getaldić had solved using the geometric method in <em>Variorum problematum</em>. In the third and fourth volume, Getaldić resolved problems by reducing them to various types of quadratic equations. In the fifth volume, he offered an important classification of problems according to the results of algebraic analysis and presented new approaches to their solution.</p><p>Although focused on the development of mathematical methods and testing their scope, <em>De resolutione </em>was often judged according to the aspect of establishing analytical geometry. Applying algebraic methods to geometrical problems, Getaldić achieved extraordinary results and paved the way for the creation of a new field of mathematics. However, he still needed one last step before establishing analytical geometry. Only a few years later, this decisive step, the first record of an algebraic representation of a geometric problem, was made by Ren&eacute; Descartes in <em>La g&eacute;om&eacute;trie</em>.</p><p>Getaldić died in Dubrovnik on 8 April 1626. He did not live to see his principal work published.</p>"
        naziv = "<em><strong>GETALDIĆ’S MASTREPIECE</strong></em>"
    } else if (slika == '2.jpg') {
        opis = "<p><strong>Marin Getaldić</strong> (Marino Ghetaldi, Marinus Ghetaldus), the most prominent Croatian mathematician and physicist at the turn of the 17<sup>th</sup> century, was born on 2 October 1568 into a Dubrovnik (Ragusa) noble family whose ancestry can be traced to the second half of the 12<sup>th</sup> century. The Getaldić family was so distinguished that several of its members were elected to the role of <em>rector</em> (<em>knez</em>), chief magistrate of the Republic of Dubrovnik. For eight centuries, the family maintained a prominent position in the public, political, diplomatic, and cultural life of Dubrovnik.</p><p>After completing his primary education, Getaldić attended the <em>Gymnasium</em> (high school) in Divona (now Sponza) Palace, in Dubrovnik, where many distinguished European humanists, philosophers, and writers taught, transferring and developing the influences of humanism and the Renaissance from neighbouring Italy. During Getaldić&rsquo;s education, the gymnasium in Dubrovnik held the rank of lyceum and the following subjects taught there: grammar, rhetoric, literature, arithmetic, physics, astronomy, philosophy, theology, law, and music. Getaldić thus attained an excellent knowledge of classical languages, with his intellectual inclinations as a youth formed in the humanist climate of Dubrovnik.</p><p>However, his family eventually fell into financial difficulties and could not afford to finance his education abroad. At the age of twenty, Getaldić became a member of the Republic&rsquo;s Grand Council and prepared to perform higher clerical and legal work in the service of the Republic. He had various public duties and occupations typical for Dubrovnik&rsquo;s elite members, and would have probably continued to perform them. Had a sudden opportunity not presented itself, he would have perhaps never devoted himself to mathematics and science. An important turnaround in his life took place in 1595, when he went on a six-year tour around Europe with his peer, Marin Gučetić. Their goal was to help settle the inheritance of Marin&rsquo;s uncle, the wealthy merchant Nikola Gučetić. Getaldić received a crucial incentive for studying mathematics and the natural sciences during his stay in European centres of science (Antwerp, London, Paris, Padua, and Rome) through contacts with the most eminent scholars of his time.</p><p>Getaldić developed and affirmed the most modern field of Renaissance mathematics &ndash; symbolic algebra. Together with his numerous new solutions, this allowed for a more exact interpretation of earlier results, offering new approaches to solving problems and paving the way for new fields. Gradually, the concept of the formula was created and the functional relations between quantities were studied, thus putting change into the focus of early modern research. The Getaldić surname was recorded in many different forms in documents and literature: <em>Ghetaldo</em>, <em>Ghetaldi</em>, <em>Ghetaldis</em>, <em>Ghettaldi</em>, <em>Gataldi</em>, <em>Gataldo</em>, <em>Getodović</em>. According to tradition, the Getaldić family moved to Dubrovnik from Taranto, southeastern Italy, in the 10<sup>th</sup> century and gradually assimilated. Getaldić was describes as: &ldquo;<em>&hellip; <strong>an angel in temperament and a demon in mathematics</strong></em>&rdquo;, by Fulgenzio Micanzio, the biographer of humanist and theologian Paolo Sarpi, on the occasion of their meeting with Galileo Galilei in Padua. Getaldić was held in high esteem during his life and his ideas were accepted among the scholarly elite of his time.</p>"
        naziv = "<em><strong>MARIN GETALDIĆ – EUROPEAN FAMOUS MATHEMATICIAN AND PHYSICIST</em>"
    } else if (slika == '3.jpg') {
        opis = "<p>Although he had shown himself as an exceptional mathematician at an early age, in addition to his scientific work Getaldić was required to perform various public duties in the service of the Republic of Dubrovnik throughout his life. He was an official in the Armaments Office and the Salt Trade Office. He supervised the reconstruction of Podzvizd fortress (1604), the highest fortress in the fortification system of Mali Ston and an important point of the Republic’s strategic interest on the Pelješac Peninsula. Following a decision of the Senate, he was appointed as one of Ston’s two captains and as the settlement’s judicial magistrate.</p><p>As a trustworthy nobleman, the Senate appointed him as envoy, so he journeyed to Constantinople (Istanbul) in 1606, bearing a tribute worth 12,500 ducats to the sultan. He spent a year in the Dubrovnik merchant colony there and served as a representative of the Republic’s interests. According to the Senate’s instructions, he requested Ottoman assistance in quelling the Lastovo Rebellion and thwarting Venetian efforts to redirect Balkan trade routes from Dubrovnik to Venetian-ruled Split. According to the preserved diplomatic letters, he requested that the Ottomans issue a warning to the Venetian ambassador that the island of Lastovo must be returned to Dubrovnik, and threaten an intervention of the Ottoman fleet should Venice fail to comply.</p><p>In addition to the diplomatic duties that he successfully completed, Getaldić measured the geographic coordinates of Constantinople more precisely than his predecessors and unsuccessfully attempted to locate the Arabic translation of Apollonius’ work on conic sections.</p><p>Many renaissance mathematicians worked on reconstructing and restoring lost treatises from classical antiquity, relying on fragments quoted in the works of other early mathematicians. Getaldić’s work was prompted by Viète’s restoration of <em>De Tactionibus</em> (<em>Tangencies</em>), a lost work written by the Greek mathematician Apollonius of Perga (third century BC). Viète reconstructed from the fragments ten of Apollonius’ mathematical problems, whilst Getaldić recognised and reconstructed a further six that were published in the work <em>Supplementum Apollonii Galli</em> (<em>A Supplement to Apollonius of Gaul</em>) in Rome in 1607. Self-consciously, Getaldić wrote at the end of his work: “<em>And thus, without Apollonius of Illyria</em> (i.e. Getaldić), <em>Apollonius of Gaul cannot revive Apollonius of Perga, who lay extinguished due to the injustice of time, or was buried by barbarians</em>.”</p><p>Examining the <em>Collection</em> of the great mathematician from classical antiquity, Pappus (third/fourth century AD), and his convoluted interpretations, Getaldić produced the first formulations of Apollonius’ problems from the lost work <em>De Inclinationibus</em> (<em>On Inclinations</em>). Although he had intended to publish his restoration in a single work, under pressure due to various obligations and his upcoming departure for diplomatic mission in the Ottoman Empire, he published the first part of the restoration under the title <em>Apollonius Redivivus</em> (<em>Apollonius Revived</em>) in Rome in 1607. It contained solutions for the first four problems, while the fifth was only formulated. Getaldić completed the fifth problem and published it in 1613, in <em>Apollonius Redivivus</em><em>, Book II.</em></p><p>The fifth problem prompted a friendly competition between Getaldić and Scottish mathematician Alexander Anderson. Each of them tried to solve the fifth problem twice, using different methods. When their restorations are compared, it is important to stress that Getaldić’s restorations in <em>Apollonius Redivivus</em> (<em>Books I &amp; II</em>) are true restorations of Apollonius’ work as they are methodologically uniform and made using geometric synthesis, which had been used in the lost original work. Getaldić’s works had a diverse and rich resonance in the natural science literature of his time.</p>"
        naziv = "<em><strong>IN THE SERVICE OF THE REPUBLIC AND MATHEMATICAL RESTORATIONS OF LOST IDEAS</em>"
    } else if (slika == '4.jpg') {
        opis = "<p>An important turnaround in Getaldić’s life took place in 1595, when he went on a six-year tour around Europe with his peer, Marin Gučetić. Their goal was to help settle the inheritance of Marin’s uncle, the wealthy merchant Nikola Gučetić. Getaldić received a crucial incentive for studying mathematics and the natural sciences during his stay in European centres of science (Antwerp, London, Paris, Padua, and Rome) through contacts with the most eminent scholars of his time. He studied mathematics under the distinguished Michel Coignet in Antwerp. In Paris, mathematician François Viète acquainted Getaldić with symbolic algebra and algebraic analysis, the most active field of mathematics at that time. In Padua, he became involved in the cultural circle gathered around Galileo Galilei.</p><p>His scholarly contacts bore much fruit; new ideas and collaboration with elite intellectual circles had a decisive influence on Getaldić’s further work. Already as a young man, he achieved a high scholarly reputation and continued his scientific work upon his return to Dubrovnik. Creating his works in his native city, he kept up-to-date with scientific research through his contacts with the most distinguished mathematicians and physicists of his time (Alexander Anderson, Christopher Clavius, Christoph Grienberger, Galileo Galilei, Federico Saminiati, Paul Gouldin, etc.) and exchanging scientific works with them.</p><p>One of Getaldić’s first works, <em>Promotus Archimedes</em> (<em>Promoted Archimedes</em>) on the relative ratios of weights, was published 1603 in Rome, as Getaldić’s only work in the field of physics grounded in experiments and mathematically modelled on Euclid’s <em>Elements</em>. The work is particularly important in the history of science because it is considered an early example of the early modern approach to natural history research.</p><p>Getaldić considered mathematics the science that most precisely describes the world. He believed in the application of experiments, as a practical aspect of science that needed to be mathematically tested and proved. Comparing various objects that differed in weight and volume (solids and liquids), he presented and solved eight problems. Getaldić systemised his conclusions into theorems, and offered examples and detailed instructions with tables showing the results of measurements made on seven solid and five liquid objects.</p><p>He was the first to construct and use for scientific purposes a device that today we recognise as a type of hydrostatic scale. Getaldić weighed items in water using a two-pan balance scale. In one pan, would be placed an object tied with horsehair so that it floats in water, while in the other were weights and an equal amount of horsehair as he had used in the first scale, because horsehair weighs almost the same as water. The starting point of his research was the traditions from classical antiquity (Archimedes, Vitruvius), which Getaldić then supplemented with the results of his own research. At the end of the work, he offered an exact solution and interpretation of Archimedes’ problem of Hiero’s golden crown. Getaldić was an accomplished experimenter and achieved significantly more precise results than his contemporaries. Some of his mathematical proofs were more elegant than even those of Galilei, who worked in the same field.</p>"
        naziv = "<em><strong>STUDY TOUR AROUND EUROPE AND COOPERATION WITH GALILEO GALILEI AND THE MOST EMINENT EUROPEAN SCIENTISTS</strong></em>"
    } else {
        opis = "<p>After returning to Dubrovnik in 1601, Getaldić continued the experiments he had begun during his travels throughout Europe. He was particularly interested in the construction of parabolic mirrors and optical experiments, which he conducted by the shore in the Betina cave, at his family estate in walking distance from Dubrovnik’s city-walls. Faced with a series of problems in geometrical optics, which showed up during the design, construction, and experimentation of parabolic mirrors, he conducted mathematical research on parabolas and published the results in <em>Nonnullae propositiones de parabola</em> (<em>Some Propositions on the Parabola</em>; Rome, 1603). Even though his work was prompted by his interests in physics, his main contribution was of a mathematical nature.</p><p>Parabolic mirrors were used to light fires by focusing sunlight and could therefore be used to determine the melting point of various materials. They could also be used to determine the position and size of images by placing an item in different positions in relation to the mirror. Getaldić constructed several parabolic mirrors, one of which has been preserved to this day. Its circumference is over two metres; made out of very thin metal, brittle glass, and reflective on both sides. After Getaldić’s death, his brother Jakov gave it to Cardinal Francesco Barberini as a gift, requesting that he support the posthumous publishing of Getaldić’s most important work, <em>De resolutione et compositione mathematica</em> (<em>On Mathematical Analysis and Synthesis</em>). For two centuries, the mirror was displayed in Barberini’s museum in Rome. Barberini had the mirror restored, and this restoration inspired poet Sante Pieralisi to write the poem <em>Lo specchio concavo sferico barberiniano</em> (<em>Barberini’s Concave Mirror</em>). Today the mirror is kept in the National Maritime Museum in Greenwich.</p><p>In Getaldić’s time, prompted by the experiments he conducted, a legend about his magical powers appeared. A century later, the legend was described by Serafin Marija Crijević in the work <em>Bibliotheca Ragusina</em> (<em>The Library of Dubrovnik</em>). Many did not understand Getaldić’s experiments and believed that the instruments in the cave were used to set fire to ships at sea. The legend was preserved through the centuries, and today the cave on the Getaldić family estate is named Betina Cave, after Getaldić’s nickname in Dubrovnik.</p>"
        naziv = "<em><strong>RETURN TO DUBROVNIK AND THE EXPERIMENTS IN THE BETINA CAVE</strong></em>"
    }

    $("#replay").fadeIn("slow")
    retci = $("#retci").val();
    stupci = $("#stupci").val();
    $(".modal").fadeOut("slow");

    // SCALING OPTIONS
    // scaling can have values as follows with full being the default
    // "fit"	sets canvas and stage to dimensions and scales to fit inside window size
    // "outside"	sets canvas and stage to dimensions and scales to fit outside window size
    // "full"	sets stage to window size with no scaling
    // "tagID"	add canvas to HTML tag of ID - set to dimensions if provided - no scaling

    var scaling = "fit"; // this will resize to fit inside the screen dimensions
    if (window.innerWidth >= 1040) {
        var width = 2000;
        var height = 1200;
    } else {
        var width = 1920;
        var height = 1600;
    }
    var countPieces = 0;
    var totalPieces = 0;
    // as of ZIM 5.5.0 you do not need to put zim before ZIM functions and classes
    var frame = new Frame(scaling, width, height);
    frame.on("ready", function() {
        zog("ready from ZIM Frame"); // logs in console (F12 - choose console)

        var stage = frame.stage;
        var stageW = frame.width;
        var stageH = frame.height;
        var label = new Label({
            text: "CLICK",
            size: 60,
            font: "Helvetica Neue",
            color: "#d0bd9f",
            rollColor: "#11676d ",
            fontOptions: "italic bold",
        });

        var label2 = new Label({
            text: "CLICK",
            size: 40,
            font: "Helvetica Neue",
            color: "#d0bd9f",
            rollColor: "#11676d",
            fontOptions: "italic bold",
        });


        stage.addChild(label);
        stage.addChild(label2);
        label.x = label.y = 20;
        label2.y = 20;
        label2.x = stageW - 240;
        label.on("click", function() {
            zog("clicking");
        });

        label2.on("click", function() {
            location.reload();
        });

        var puzzleX;
        var puzzleY;
        frame.outerColor = "#707070";
        frame.color = "#FFF";

        var con = new Container

        // with chaining - can also assign to a variable for later access
        var imageObj = [];
        var piecesArrayObj = [];
        frame.loadAssets([slika], "assets/");



        frame.on("complete", function() {
            imageObj = frame.asset(slika).clone();
            imageObj.addTo(con);
            imageObj.alpha = 0.2;

            var piecesArray = new Array();
            var horizontalPieces = retci;
            var verticalPieces = stupci;
            var obj = getQueryString();
            zog(obj)
            if (obj) {
                horizontalPieces = obj.row;
                verticalPieces = obj.column;
            }
            var imageWidth = imageObj.width;
            var imageHeight = imageObj.height;
            var pieceWidth = Math.round(imageWidth / horizontalPieces);
            var pieceHeight = Math.round(imageHeight / verticalPieces);
            var gap = 40;
            totalPieces = horizontalPieces * verticalPieces;

            puzzleX = frame.width / 2 - imageWidth / 2;
            puzzleY = frame.height / 2 - imageHeight / 2;
            imageObj.pos(puzzleX, puzzleY);
            zog(puzzleX, puzzleY);


            label.text = countPieces + "/" + totalPieces;
            label2.text = "New game"

            for (j = 0; j < verticalPieces; j++) {
                piecesArrayObj[j] = [];
                for (i = 0; i < horizontalPieces; i++) {
                    var n = j + i * verticalPieces;

                    var offsetX = pieceWidth * i;
                    var offsetY = pieceHeight * j;


                    var x8 = Math.round(pieceWidth / 8);
                    var y8 = Math.round(pieceHeight / 8);

                    piecesArrayObj[j][i] = new Object();
                    piecesArrayObj[j][i].right = Math.floor(Math.random() * 2);
                    piecesArrayObj[j][i].down = Math.floor(Math.random() * 2);

                    if (j > 0) {
                        piecesArrayObj[j][i].up = 1 - piecesArrayObj[j - 1][i].down;
                    }
                    if (i > 0) {
                        piecesArrayObj[j][i].left = 1 - piecesArrayObj[j][i - 1].right;
                    }

                    piecesArray[n] = new Rectangle({
                        width: pieceWidth,
                        height: pieceHeight,

                    });

                    var tileObj = piecesArrayObj[j][i];
                    var s = new Shape

                    var context = s.graphics;
                    s.drag();
                    s.mouseChildren = false;
                    s.addEventListener("pressup", function(e) {
                        var mc = e.currentTarget;

                        var xx = Math.round(mc.x);
                        var yy = Math.round(mc.y);

                        if (xx < puzzleX + gap / 2 && xx > puzzleX - gap / 2 && yy < puzzleX + gap / 2 && yy > puzzleY - gap / 2) {
                            mc.x = puzzleX;
                            mc.y = puzzleY;
                            mc.noDrag();
                            mc.addTo(mc.parent, 0);
                            mc.mouseChildren = false;
                            mc.mouseEnabled = false;
                            mc.hint.visible = false;
                            countPieces++;
                            label.text = countPieces + "/" + totalPieces;
                            zog("countPieces", countPieces);
                            if (countPieces == totalPieces) {

                                swal({
                                    html: '<h1>' + naziv + '</h1><img src="assets/' + slika + '" class="ikone2"/>'+opis,
                                    confirmButtonText: "New game",
                                    confirmButtonColor: '#008186',
                                    allowOutsideClick: true,
                                });
                                $('.swal2-confirm').click(function() {
                                    location.reload();
                                });
                                $('.ikone2').lightzoom({
                                    glassSize: 175,
                                    zoomPower: 2
                                });

                                document.getElementsByClassName('ikone2')[0].addEventListener("touchstart", touchHandler, true);
                                document.getElementsByClassName('ikone2')[0].addEventListener("touchmove", touchHandler, true);
                                document.getElementsByClassName('ikone2')[0].addEventListener("touchend", touchHandler, true);
                                document.getElementsByClassName('ikone2')[0].addEventListener("touchcancel", touchHandler, true);
                            };

                            stage.update();

                        }

                    });
                    context.setStrokeStyle(3, "round");
                    var commandi1 = context.beginStroke(createjs.Graphics.getRGB(0, 0, 0)).command;
                    //
                    var commandi = context.beginBitmapFill(imageObj.image).command;


                    context.moveTo(offsetX, offsetY);

                    if (j != 0) {
                        context.lineTo(offsetX + 3 * x8, offsetY);
                        if (tileObj.up == 1) {
                            context.curveTo(offsetX + 2 * x8, offsetY - 2 * y8, offsetX + 4 * x8, offsetY - 2 * y8);
                            context.curveTo(offsetX + 6 * x8, offsetY - 2 * y8, offsetX + 5 * x8, offsetY);
                        } else {
                            context.curveTo(offsetX + 2 * x8, offsetY + 2 * y8, offsetX + 4 * x8, offsetY + 2 * y8);
                            context.curveTo(offsetX + 6 * x8, offsetY + 2 * y8, offsetX + 5 * x8, offsetY);
                        }
                    }
                    context.lineTo(offsetX + 8 * x8, offsetY);
                    if (i != horizontalPieces - 1) {
                        context.lineTo(offsetX + 8 * x8, offsetY + 3 * y8);
                        if (tileObj.right == 1) {
                            context.curveTo(offsetX + 10 * x8, offsetY + 2 * y8, offsetX + 10 * x8, offsetY + 4 * y8);
                            context.curveTo(offsetX + 10 * x8, offsetY + 6 * y8, offsetX + 8 * x8, offsetY + 5 * y8);
                        } else {
                            context.curveTo(offsetX + 6 * x8, offsetY + 2 * y8, offsetX + 6 * x8, offsetY + 4 * y8);
                            context.curveTo(offsetX + 6 * x8, offsetY + 6 * y8, offsetX + 8 * x8, offsetY + 5 * y8);
                        }
                    }
                    context.lineTo(offsetX + 8 * x8, offsetY + 8 * y8);
                    if (j != verticalPieces - 1) {
                        context.lineTo(offsetX + 5 * x8, offsetY + 8 * y8);
                        if (tileObj.down == 1) {
                            context.curveTo(offsetX + 6 * x8, offsetY + 10 * y8, offsetX + 4 * x8, offsetY + 10 * y8);
                            context.curveTo(offsetX + 2 * x8, offsetY + 10 * y8, offsetX + 3 * x8, offsetY + 8 * y8);
                        } else {
                            context.curveTo(offsetX + 6 * x8, offsetY + 6 * y8, offsetX + 4 * x8, offsetY + 6 * y8);
                            context.curveTo(offsetX + 2 * x8, offsetY + 6 * y8, offsetX + 3 * x8, offsetY + 8 * y8);
                        }
                    }
                    context.lineTo(offsetX, offsetY + 8 * y8);
                    if (i != 0) {
                        context.lineTo(offsetX, offsetY + 5 * y8);
                        if (tileObj.left == 1) {
                            context.curveTo(offsetX - 2 * x8, offsetY + 6 * y8, offsetX - 2 * x8, offsetY + 4 * y8);
                            context.curveTo(offsetX - 2 * x8, offsetY + 2 * y8, offsetX, offsetY + 3 * y8);
                        } else {
                            context.curveTo(offsetX + 2 * x8, offsetY + 6 * y8, offsetX + 2 * x8, offsetY + 4 * y8);
                            context.curveTo(offsetX + 2 * x8, offsetY + 2 * y8, offsetX, offsetY + 3 * y8);
                        }
                    }
                    context.lineTo(offsetX, offsetY);
                    s.addTo(con);

                    var fill = new createjs.Graphics.Fill("red");

                    //var newGra = context.append(fill);
                    var hint = new Shape(); //s.clone(true);
                    hint.mouseChildren = false;
                    hint.mouseEnabled = false;
                    s.hint = hint;
                    hint.graphics = context.clone(true);
                    hint.pos(puzzleX, puzzleY);
                    // newGra.graphics = newGra;
                    hint.graphics._fill = fill;
                    hint.graphics._fill.style = null;

                    hint.addTo(con, 0);
                    //s.animate({obj:{x:frame.width-offsetX-pieceWidth,y:frame.height-offsetY-pieceHeight}, time:700});
                    //s.animate({obj:{x:-offsetX,y:-offsetY}, time:700});
                    s.animate({
                        obj: {
                            x: rand(-offsetX, frame.width - offsetX - pieceWidth),
                            y: rand(-offsetY, frame.height - offsetY - pieceHeight)
                        },
                        time: 700
                    });

                }
            }


            con.addTo(stage);
            /*con.x -= imageWidth/2;
            con.y -= imageHeight/2;*/
            stage.update();



        }); // end asset complete


        stage.update(); // this is needed to show any changes

    }); // end of ready
}


function touchHandler(event) {
    var touches = event.changedTouches,
        first = touches[0],
        type = "";
    switch (event.type) {
        case "touchstart":
            type = "mousedown";
            break;
        case "touchmove":
            type = "mousemove";
            break;
        case "touchend":
            type = "mouseup";
            break;
        default:
            return;
    }


    // initMouseEvent(type, canBubble, cancelable, view, clickCount, 
    //                screenX, screenY, clientX, clientY, ctrlKey, 
    //                altKey, shiftKey, metaKey, button, relatedTarget);

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent(type, true, true, window, 1,
        first.screenX, first.screenY,
        first.clientX, first.clientY, false,
        false, false, false, 0 /*left*/ , null);

    first.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}



$(document).mouseup(function(e) {
    var container = $(".ikone2");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {

        $("#glass").hide();
    }

});