// Memory Game
// © 2014 Nate Wiley
// License -- MIT
// best in full screen, works on phones/tablets (min height for game is 500px..) enjoy ;)
// Follow me on Codepen

var razina = 1;
var broj_karata = 3;

zastave = '<div class="centar"> </div>'

$(".modal").html(zastave + "<h2 class='winner'>Chose the number of pairs:</h2><button id='prva'>4</button> <button id='druga'>8</button>");


$("#prva").click(function () {
    razina = "1";
    igra()
})
$("#druga").click(function () {
    razina = "2";
    igra()
})
/*$("#treca").click(function() {
    razina = "3";
    igra()
})*/


function igra() {
    if (razina == 1) {
        broj_karata = 4;

    } else if (razina == 2) {
        broj_karata = 8;
    } else {
        broj_karata = 12
    }
    $("footer").fadeIn(1000);
    $(".modal").fadeOut(1000);
    $(".modal-overlay").delay(1000).slideUp(1000);
    $(".game").show("slow");
    //localStorage.clear();

    var br = 1;
    var sec = 0;
    var pokusaj = 0;
    var vrijeme = 1;

    var najbolje_vrijeme;
    var najmanji_broj_pokusaja;
    var karte;

    function pad(val) {
        return val > 9 ? val : "0" + val;
    }
    setInterval(function () {
        if (vrijeme == 1) {
            $("#seconds").html(pad(++sec % 60));
            $("#minutes").html(pad(parseInt(sec / 60, 10)));
        }
    }, 1000);
    var Memory = {

        init: function (cards) {
            this.$game = $(".game");
            this.$modal = $(".modal");
            this.$overlay = $(".modal-overlay");
            this.$restartButton = $(".restart");
            this.cardsArray = $.merge(cards, cards);

            this.shuffleCards(this.cardsArray);

            this.setup();
        },

        shuffleCards: function (cardsArray) {
            this.$cards = $(this.shuffle(this.cardsArray));
        },

        setup: function () {
            this.html = this.buildHTML();
            this.$game.html(this.html);
            this.$memoryCards = $(".card");
            this.binding();
            this.paused = false;
            this.guess = null;
            this.$cards = $(this.shuffle(this.cardsArray));

        },

        binding: function () {
            this.$memoryCards.on("click", this.cardClicked);
            this.$restartButton.on("click", $.proxy(this.reset, this));
        },
        // kinda messy but hey
        cardClicked: function () {
            var _ = Memory;
            var $card = $(this);
            if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

                $card.find(".inside").addClass("picked");
                if (!_.guess) {
                    _.guess = $(this).attr("data-id");
                    $(this).find('p').toggle();
                } else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
                    $(".picked").addClass("matched");
                    _.guess = null;
                    $(".matched").find('p').remove();
                    pokusaj++;
                    switch ($(this).attr('data-id')) {
                        case "1":
                            vrijeme = 0;
                            swal({
                                title: 'The Author of the  Significant Mathematical Work',
                                html: '<br><img src="slike/1.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Marin Getaldic,who was the greatest mathematician and physicist at the turn of the 17<sup>th</sup> century,established and developed the symbolic algebra &ndash; the most recent discipline in the field of the renaissance mathematics of that time. <br /> He published one book on physics and six mathematical books which resonated with his contemporaries and beyond. Getaldic strongly believed that the application of mathematics can assist in exploring and defining the world that is enveloping us</p>',
                                showCloseButton: true,
                                confirmButtonText: 'continue'
                                /*allowOutsideClick: false,*/
                                /*allowEscapeKey: false*/

                            }, function (isConfirm) {

                            });
                            $(document).keydown(function (e) {
                                if (e.which == 27) {
                                    vrijeme = 1;
                                }
                                else if (e.which == 13) {
                                    vrijeme = 1;
                                }
                            }); $('.swal2-container').on('click', function (event) {
                                if ($(event.target).closest(".swal2-modal").length === 0) {
                                    vrijeme = 1;
                                }
                            });
                            $('.swal2-confirm, .swal2-close').click(function () {
                                vrijeme = 1;
                            });
                            break;
                        case "2":
                            vrijeme = 0;
                            swal({
                                title: 'Mathematician and Physicist',
                                html: '<br><img src="slike/2.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Marin Getaldic was educated in his native town of Dubrovnik where he produced his magnum opus in mathematics and physics. He was living at the beginning of an era of modern science, one of the most significant period in our civilization. By experimental work and application of mathematics in physics he contributed to the shaping and development of modern science and offered new mathematical methods and a new approach to researching as well.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'continue'
                                /*allowOutsideClick: false,*/
                                /*allowEscapeKey: false*/

                            }, function (isConfirm) {

                            });
                            $(document).keydown(function (e) {
                                if (e.which == 27) {
                                    vrijeme = 1;
                                }
                                else if (e.which == 13) {
                                    vrijeme = 1;
                                }
                            }); $('.swal2-container').on('click', function (event) {
                                if ($(event.target).closest(".swal2-modal").length === 0) {
                                    vrijeme = 1;
                                }
                            });

                            $('.swal2-confirm, .swal2-close').click(function () {
                                vrijeme = 1;
                            });
                            break;
                        case "3":
                            vrijeme = 0;
                            swal({
                                title: 'The Nobleman',
                                html: '<br><img src="slike/3.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Getaldic was born to a noble family whose ancestors could be traced down to the late 12<sup>th</sup> century . For eight centuries the family has been playing a prominent role in the public, political, diplomatic and cultural life of Dubrovnik. In the past some members of the family who were chosen to be given a honorary title governed the Republic of Dubrovnik as princes.</p>',
                                confirmButtonText: 'continue'
                                /*allowOutsideClick: false,*/
                                /*allowEscapeKey: false*/


                            }, function (isConfirm) {

                            }); $(document).keydown(function (e) {
                                if (e.which == 27) {
                                    vrijeme = 1;
                                }
                                else if (e.which == 13) {
                                    vrijeme = 1;
                                }
                            }); $('.swal2-container').on('click', function (event) {
                                if ($(event.target).closest(".swal2-modal").length === 0) {
                                    vrijeme = 1;
                                }
                            });
                            $('.swal2-confirm, .swal2-close').click(function () {
                                vrijeme = 1;
                            });
                            break;
                        case "4":
                            vrijeme = 0;
                            swal({
                                title: 'Getaldic`s  Mastrepiece',
                                html: '<br><img src="slike/4.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Getaldic took most interest in developing mathematical methods and their application in the different fields . In his early work he utilises ancient mathematical methods to eventually develop a new symbolic algebra. <br /> In his masterpiece &ldquo;On Mathematical Analysis and Synthesis&rdquo;<br /> (&ldquo;<em>O</em> <em>matematickoj analizi i sintezi</em>&ldquo;), the first book ever in algebraic analysis he comes close to the discovery of a new mathematical branch &ndash; the analytical geometry.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'continue'
                                /*allowOutsideClick: false,*/
                                /*allowEscapeKey: false*/


                            }, function (isConfirm) {

                            }); $(document).keydown(function (e) {
                                if (e.which == 27) {
                                    vrijeme = 1;
                                }
                                else if (e.which == 13) {
                                    vrijeme = 1;
                                }
                            }); $('.swal2-container').on('click', function (event) {
                                if ($(event.target).closest(".swal2-modal").length === 0) {
                                    vrijeme = 1;
                                }
                            });
                            $('.swal2-confirm, .swal2-close').click(function () {
                                vrijeme = 1;


                            });
                            break;
                        case "5":
                            vrijeme = 0;
                            swal({
                                title: 'Service with the Republic of Dubrovnik',
                                html: '<br><img src="slike/5.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">From an early age Getaldic proved himself an outstanding mathematician and dedicated his entire life to science. However, in order to earn his living Getaldic had to take the different jobs while serving the Republic of Dubrovnik. Among other jobs, he worked for an Ordnance office and was also engaged in the sale of salt. He was actively involved in the renovation of the Pozvizd fortress ( in 1604), the highest in the fortification system of Mali Ston and a very important strategic point in the Republic. In Ston he used to be a captain as well as its court administrator. Nominated by the Senate as an envoy to take the tribute to the sultan in Konstantinople Getaldic departed in 1607 to stay there for a year representing the Republic of Dubrovnik and its interests.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'continue'
                                /*allowOutsideClick: false,*/
                                /*allowEscapeKey: false*/


                            }, function (isConfirm) {

                            }); $(document).keydown(function (e) {
                                if (e.which == 27) {
                                    vrijeme = 1;
                                }
                                else if (e.which == 13) {
                                    vrijeme = 1;
                                }
                            }); $('.swal2-container').on('click', function (event) {
                                if ($(event.target).closest(".swal2-modal").length === 0) {
                                    vrijeme = 1;
                                }
                            });
                            $('.swal2-confirm, .swal2-close').click(function () {
                                vrijeme = 1;

                            });
                            break;
                        case "6":
                            vrijeme = 0;
                            swal({
                                title: 'The Divone Palace – Dubrovnik High School',
                                html: '<br><img src="slike/6.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Getaldic was educated in Dubrovnik, his native town. After completing his primary education he attended the secondary School in Dubrovnik situated in the palace called Divone, today`s Sponza. The good education was highly appreciated across all population groups so the teachers employed were highly respectable humanists from Europe.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'continue'
                                /*allowOutsideClick: false,*/
                                /*allowEscapeKey: false*/


                            }, function (isConfirm) {

                            }); $(document).keydown(function (e) {
                                if (e.which == 27) {
                                    vrijeme = 1;
                                }
                                else if (e.which == 13) {
                                    vrijeme = 1;
                                }
                            }); $('.swal2-container').on('click', function (event) {
                                if ($(event.target).closest(".swal2-modal").length === 0) {
                                    vrijeme = 1;
                                }
                            });
                            $('.swal2-confirm, .swal2-close').click(function () {
                                vrijeme = 1;

                            });
                            break;
                        case "7":
                            vrijeme = 0;
                            swal({
                                title: 'Cooperation with Galileo Galilei and Vieta – the most eminent European Scientists',
                                html: '<br><img src="slike/7.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Getaldic had an unusual &nbsp;path to science and the prominent scientific circles.<br /> He travelled Europe from 1595 to 1561 and visited Antwerp, London, Paris, Padua and Rome ; he was there on a mission to assist the inheritance settlement of a rich merchant of Dubrovnik, named Nikola Gucetic. <br /> Getaldic studied mathematics and received crucial enticements to take interest in natural sciences .It was there that he met the most prominent scholars of the time, including Galileo Galilei Francois Viete and others; they would be the ones he would be exchanging ideas and printed matter with after he returned to Dubrovnik and continued with his scientific work.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'continue'
                                /*allowOutsideClick: false,*/
                                /*allowEscapeKey: false*/


                            }, function (isConfirm) {

                            }); $(document).keydown(function (e) {
                                if (e.which == 27) {
                                    vrijeme = 1;
                                }
                                else if (e.which == 13) {
                                    vrijeme = 1;
                                }
                            }); $('.swal2-container').on('click', function (event) {
                                if ($(event.target).closest(".swal2-modal").length === 0) {
                                    vrijeme = 1;
                                }
                            });
                            $('.swal2-confirm, .swal2-close').click(function () {
                                vrijeme = 1;


                            });
                            break;
                        case "8":
                            vrijeme = 0;
                            swal({
                                title: 'The Experiments in the Caves of Betina',
                                html: '<br><img src="slike/8.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">On his return to Dubrovnik in 1601, Getaldic pursued his experimental work that he had started in Europe. His special interest was the construction of a parabolic mirror along with the optical experimentation ; all was trialled in a cave on the family estate at the foot of the Srd hill in the area called Ploce. <br /> These experiments might have been the inspiration to a legend about the wizard Beta who<strong>, </strong>using some instruments<strong>, </strong>was setting on flames the hostile vessels approaching the city walls. </p>',
                                showCloseButton: true,
                                confirmButtonText: 'continue'
                                /*allowOutsideClick: false,*/
                                /*allowEscapeKey: false*/


                            }, function (isConfirm) {

                            }); $(document).keydown(function (e) {
                                if (e.which == 27) {
                                    vrijeme = 1;
                                }
                                else if (e.which == 13) {
                                    vrijeme = 1;
                                }
                            }); $('.swal2-container').on('click', function (event) {
                                if ($(event.target).closest(".swal2-modal").length === 0) {
                                    vrijeme = 1;
                                }
                            });
                            $('.swal2-confirm, .swal2-close').click(function () {
                                vrijeme = 1;
                            });
                            break;
                        /*  case "9":
                              vrijeme = 0;
                              swal({
                                  title: 'Rog noćnih stražara<br>Aljmaš, 2. pol. 19. st.',
                                  html: '<br><img src="slike/rog-nocnih-strazara.jpg" class="ikone"/>' +
                                      '<p style="text-align:justify;">Životinjski rogovi kao što su <em>kravarski rog, rog noćnih stražara i kozarski rog</em> izrađeni su od roga vola, bivola ili koze. Rog bi se odstranio sa životinje, dobro očistio s unutarnje i vanjske strane te dodatno ostrugao suhom travom kako bi bio što glađi. <em>Rog noćnih stražara</em>, za razliku od druga dva, ima još dodatni pisak od trske koji se umeće u tanji otvor te pričvršćuje lanom i lijepi voskom.</p>',
                                  showCloseButton: true,
                                  confirmButtonText: 'continue'
                          


                              }, function(isConfirm) {

                              });
                              $('.swal2-confirm, .swal2-close').click(function() {
                                  vrijeme = 1;
  

                              });
                              break;*/
                        /* case "10":
                             vrijeme = 0;
                             swal({
                                 title: 'Lira - lirica <br>Blato, Korčula - 1. pol. 20. st. ',
                                 html: '<br><img src="slike/Lira.jpg" class="ikone"/>' +
                                     '<p style="text-align:justify;">Lira ili lijerica solističko je glazbalo namijenjeno ponajprije glazbenoj pratnji plesa, primjerice <em>linđa</em> ili <em>poskočice</em>.  Lijerica je i danas u živoj uporabi u Dubrovačkom primorju, Konavlima i na poluotoku Pelješcu te na otocima Mljetu i Lastovu.</p>',
                                 showCloseButton: true,
                                 confirmButtonText: 'continue'
                               

                             }, function(isConfirm) {

                             });
                             $('.swal2-confirm, .swal2-close').click(function() {
                                 vrijeme = 1;
 

                             });
                             break;*/
                        /* case "11":
                            vrijeme = 0;
                            swal({
                                title: 'Bubanj <br>Ivankovo, 1. pol. 20. st.',
                                html: '<br><img src="slike/Buba.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Bubanj iz Ivankova izrađen je iz jednog komada drveta unutar kojeg je kao opna navučena janjeća koža, a svira se uz pomoć dvaju batića kojima se udara po opni. Ovo se glazbalo koristilo kao pratnja <em>dvojnicama</em> ili <em>tamburama dvožicama</em> na većim slavljima i u svatovima.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'continue'
                                allowOutsideClick: false

                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm, .swal2-close').click(function() {
                                vrijeme = 1;
 

                            });
                            break;
                        case "12":
                            vrijeme = 0;
                            swal({
                                title: 'Tambura – bugarija<br>Rijeka, oko 1900.',
                                html: '<br><img src="slike/Tambura.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Tambura je trzalačko kordofono glazbalo na kojem se zvuk proizvodi trzanjem trzalicom po žicama. Sastoji se od tri osnovna dijela: trupa, vrata s hvataljkom i glave. Trup (koji se još naziva i <em>korpus, zvekalo, tijelo</em>) može biti izrađen iz izdubljenog komada drveta klena, šljive, javora, duda, oraha, jasena i slično.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'continue'
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm, .swal2-close').click(function() {
                                vrijeme = 1;
 

                            });
                            break;*/



                    }

                } else {
                    pokusaj++;
                    $(this).find('p').toggle();
                    _.guess = null;
                    _.paused = true;
                    setTimeout(function () {
                        $(".picked").removeClass("picked");
                        Memory.paused = false;
                        $(".brojevi").show();
                    }, 1200);
                }
                if ($(".matched").length == $(".card").length) {
                    _.win();
                }
            }
        },

        win: function () {
            this.paused = true;
            setTimeout(function () {
                Memory.showModal();
                Memory.$game.fadeOut();

            }, 1000);
        },

        showModal: function () {
            var minute = Math.floor(sec / 60);
            var sekunde = sec - minute * 60;
            this.$overlay.show();
            this.$modal.fadeIn("slow");

            if (razina == 1) {
                var najvrijeme = localStorage.getItem('najvrijeme');
                if (najvrijeme === undefined || najvrijeme === null) {
                    najvrijeme = sec;
                    localStorage.setItem('najvrijeme', sec);
                }
                // If the user has more points than the currently stored high score then
                if (sec < najvrijeme) {
                    // Set the high score to the users' current points
                    najvrijeme = sec;
                    // Store the high score
                    localStorage.setItem('najvrijeme', sec);
                }


                var najpokusaji = localStorage.getItem('najpokusaji');

                if (najpokusaji === undefined || najpokusaji === null) {
                    najpokusaji = pokusaj;
                    localStorage.setItem('najpokusaji', pokusaj);
                }

                // If the user has more points than the currently stored high score then
                if (pokusaj < najpokusaji) {
                    // Set the high score to the users' current points
                    najpokusaji = pokusaj;
                    // Store the high score
                    localStorage.setItem('najpokusaji', najpokusaji);
                }
            } else if (razina == 2) {
                var najvrijeme = localStorage.getItem('najvrijeme2');
                if (najvrijeme === undefined || najvrijeme === null) {
                    najvrijeme = sec;
                    localStorage.setItem('najvrijeme2', sec);
                }
                // If the user has more points than the currently stored high score then
                if (sec < najvrijeme) {
                    // Set the high score to the users' current points
                    najvrijeme = sec;
                    // Store the high score
                    localStorage.setItem('najvrijeme2', sec);
                }

                var najpokusaji = localStorage.getItem('najpokusaji2');

                if (najpokusaji === undefined || najpokusaji === null) {
                    najpokusaji = pokusaj;
                    localStorage.setItem('najpokusaji2', pokusaj);
                }

                // If the user has more points than the currently stored high score then
                if (pokusaj < najpokusaji) {
                    // Set the high score to the users' current points
                    najpokusaji = pokusaj;
                    // Store the high score
                    localStorage.setItem('najpokusaji2', pokusaj);
                }
            } else {
                var najvrijeme = localStorage.getItem('najvrijeme3');
                if (najvrijeme === undefined || najvrijeme === null) {
                    najvrijeme = sec;
                    localStorage.setItem('najvrijeme3', sec);
                }
                // If the user has more points than the currently stored high score then
                if (sec < najvrijeme) {
                    // Set the high score to the users' current points
                    najvrijeme = sec;
                    // Store the high score
                    localStorage.setItem('najvrijeme3', sec);
                }


                var najpokusaji = localStorage.getItem('najpokusaji3');

                if (najpokusaji === undefined || najpokusaji === null) {
                    najpokusaji = pokusaj;
                    localStorage.setItem('najpokusaji3', pokusaj);
                }

                // If the user has more points than the currently stored high score then
                if (pokusaj < najpokusaji) {
                    // Set the high score to the users' current points
                    najpokusaji = pokusaj;
                    // Store the high score
                    localStorage.setItem('najpokusaji3', pokusaj);
                }
            }

            // Return the high score

            var naj_minute = Math.floor(najvrijeme / 60);
            var naj_sekunde = najvrijeme - naj_minute * 60;
            $(".modal").show();
            $(".modal-overlay").show();
            $(".winner").hide();
            $(".modal").html("<div class='winner'>You finished the game!</div><div class='time'><br>number of attempts: " + pokusaj + "</br>least number of attempts on this level: " + najpokusaji + "<br></br></br>game time: " + minute + ":" + sekunde + "<br>best time on this level: " + naj_minute + ":" + naj_sekunde + "<br><br><br><a id='reset' onclick='window.location.reload()'>New game</a></p></div>")
        },

        hideModal: function () {
            this.$overlay.hide();
            this.$modal.hide();
        },

        reset: function () {
            this.hideModal();
            this.shuffleCards(this.cardsArray);
            this.setup();
            this.$game.show("slow");
            pokusaj = 0;
            sec = 0;
            br = 1;
        },

        // Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
        shuffle: function (array) {
            var counter = array.length,
                temp, index;
            // While there are elements in the array
            while (counter > 0) {
                // Pick a random index
                index = Math.floor(Math.random() * counter);
                // Decrease counter by 1
                counter--;
                // And swap the last element with it
                temp = array[counter];
                array[counter] = array[index];
                array[index] = temp;
            }
            return array;
        },

        buildHTML: function () {
            var frag = '';
            br = 1;
            this.$cards.each(function (k, v) {
                frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
      <div class="front"><img src="' + v.img + '"\
      alt="' + v.name + '" /></div>\
      <div class="back"><p class="brojevi">' + br + '</p></div></div>\
      </div>';
                if (br < cards.length) {
                    br++;
                };
            });
            return frag;
        }
    };

    var cards = [{
        name: "",
        img: "slike/1.jpg",
        id: 1,
    }, {
        name: "",
        img: "slike/2.jpg",
        id: 2
    }, {
        name: "",
        img: "slike/3.jpg",
        id: 3
    }, {
        name: "",
        img: "slike/4.jpg",
        id: 4
    }, {
        name: "",
        img: "slike/5.jpg",
        id: 5
    }, {
        name: "",
        img: "slike/6.jpg",
        id: 6
    }, {
        name: "",
        img: "slike/7.jpg",
        id: 7
    }, {
        name: "",
        img: "slike/8.jpg",
        id: 8
    }
        /*, {
                name: "",
                img: "slike/9.jpg",
                id: 9
            }, {
                name: "",
                img: "slike/Lira.jpg",
                id: 10
            }, {
                name: "Bubanj Ivankovo, 1. pol. 20. st.",
                img: "slike/Buba.jpg",
                id: 11
            }, {
                name: "Tambura bugarija Rijeka, oko 1900.",
                img: "slike/Tambura.jpg",
                id: 12
            }*/
    ];

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    cards = shuffle(cards);

    cards = cards.slice(0, broj_karata);

    var brojKarata = cards.length;
    Memory.init(cards);

    if (razina == 1) {
        $(".card").css({
            "width": "25%",
            "height": "50%"
        })
    } else if (razina == 2) {
        $(".card").css({
            "width": "25%",
            "height": "25%"
        })
    } else if (razina == 3) {
        $(".card").css({
            "width": "16.66666%",
            "height": "25%"
        })
    }
}